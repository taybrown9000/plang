package es.cleffongam.plang.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import es.cleffongam.plang.PlangMain;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(1024, 600);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new PlangMain();
        }
}