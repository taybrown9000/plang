package es.cleffongam.plang.desktop;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import es.cleffongam.plang.PlangMain;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		// my config
		config.title = "Plang";
		
		// Nexus 7 res
		config.width = 1024;
		config.height = 600;
		
		
		config.addIcon("icons/icon_128.png", FileType.Internal);
		config.addIcon("icons/icon_32.png", FileType.Internal);
		config.addIcon("icons/icon_16.png", FileType.Internal);
		
		new LwjglApplication(new PlangMain(), config);
	}
}
