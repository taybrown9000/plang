package es.cleffongam.plang;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;

import es.cleffongam.plang.game.Assets;
import es.cleffongam.plang.game.Cameras;
import es.cleffongam.plang.screens.TitleScreen;

public class PlangMain extends Game {
	
	@Override
	public void create () {
		// Set libgdx log level
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		
		// Initialize assets
		Assets.instance.init(new AssetManager());
		
		// initialize cameras
		Cameras.instance.init();
		
		// Start at game screen
		setScreen(new TitleScreen(this));
//		setScreen(new GameScreen(this));
	}

}
