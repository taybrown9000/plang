package es.cleffongam.plang.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import es.cleffongam.plang.game.Assets;
import es.cleffongam.plang.game.WorldController;
import es.cleffongam.plang.game.WorldRenderer;

public class GameScreen extends AbstractGameScreen {
	
	public static final String TAG = GameScreen.class.getName();
	
	private WorldController worldController;
	private WorldRenderer worldRenderer;
	
	private boolean paused;

	public GameScreen(Game game) {
		super(game);

		worldController = new WorldController(game);
		worldRenderer = new WorldRenderer(worldController);
	}

	@Override
	public void render(float deltaTime) {
		
		if (!paused) {
			worldController.update();
		}
		
		// Set background to black
		Gdx.gl.glClearColor(0, 0, 0, 1);
		// Clears the screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		worldRenderer.render();
		
		if (worldController.isGameOver()) {
			Assets.instance.sounds.gameOver.play();
			game.setScreen(new GameOverScreen(game, worldController.level));
			dispose();
		}
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void show() {
		Assets.instance.music.bg_music.play();
		Assets.instance.music.bg_music.setLooping(true);
		Assets.instance.music.bg_music.setVolume(0.2f);
		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public void hide() {
		Assets.instance.music.bg_music.stop();
		Assets.instance.sounds.paddleLaser.stop();
		Gdx.input.setCatchBackKey(false);
	}

	@Override
	public void pause() {
		super.pause();
		paused = true;
	}

	@Override
	public void resume() {
		super.resume();
		// Android only
		paused = false;
	}

	@Override
	public void dispose() {
		worldRenderer.dispose();
		Assets.instance.sounds.paddleLaser.dispose();
		Assets.instance.music.bg_music.dispose();
		
	}

}
