package es.cleffongam.plang.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import es.cleffongam.plang.game.Assets;
import es.cleffongam.plang.game.Cameras;
import es.cleffongam.plang.game.objects.Stars;

public class TitleScreen extends AbstractGameScreen {
	
	private Stars stars;
	private ShapeRenderer shapeRenderer;
	private SpriteBatch batch;
	private BitmapFont titleFont;
	
	public TitleScreen(Game game) {
		super(game);
		
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		titleFont = Assets.instance.fonts.guiFont;
		stars = new Stars();
	}

	@Override
	public void render(float deltaTime) {
		batch.setProjectionMatrix(
				Cameras.instance.guiCamera.combined);
		shapeRenderer.setProjectionMatrix(
				Cameras.instance.worldCamera.combined);
		
		// Set background to black
		Gdx.gl.glClearColor(0, 0, 0, 1);
		// Clears the screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		shapeRenderer.begin(ShapeType.Filled);
		stars.render(shapeRenderer);
		shapeRenderer.end();
		
		stars.update();
		
		renderTitle(batch);
		
		if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game));
            dispose();
        }
	}

	private void renderTitle(SpriteBatch batch) {
		float x = Cameras.instance.guiCamera.viewportWidth / 2;
		float y = Cameras.instance.guiCamera.viewportHeight / 2;
		
		batch.begin();
		titleFont.setScale(6f);
		titleFont.draw(batch, "PLANG", x - 290, y + 100);
		titleFont.setScale(1.3f);
		titleFont.draw(batch, "Tap to begin", x - 135, y - 80);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		shapeRenderer.dispose();
		batch.dispose();
		stars.dispose();
	}
}
