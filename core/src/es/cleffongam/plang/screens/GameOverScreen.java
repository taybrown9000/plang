package es.cleffongam.plang.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;

import es.cleffongam.plang.game.Assets;
import es.cleffongam.plang.game.Cameras;
import es.cleffongam.plang.game.Level;

public class GameOverScreen extends AbstractGameScreen implements GestureListener {
	
	private BitmapFont scoreFont;
	private SpriteBatch batch;
	
	private Level level;

	public GameOverScreen(Game game) {
		super(game);
		
		batch = new SpriteBatch();
		scoreFont = Assets.instance.fonts.guiFont;
	}
	
	// use this one
	public GameOverScreen(Game game, Level level) {
		super(game);
		this.level = level;
		
		GestureDetector gd = new GestureDetector(this);
		Gdx.input.setInputProcessor(gd);
		
		batch = new SpriteBatch();
		scoreFont = Assets.instance.fonts.guiFont;
	}

	@Override
	public void render(float deltaTime) {
		
		batch.setProjectionMatrix(
				Cameras.instance.guiCamera.combined);
	
		// Set background to black
		Gdx.gl.glClearColor(0, 0, 0, 1);
		// Clears the screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		renderFont();
	}

	private void renderFont() {
		float x = Cameras.instance.guiCamera.viewportWidth / 2;
		float y = Cameras.instance.guiCamera.viewportHeight / 2;
		
		batch.begin();
		scoreFont.setScale(5f);
		scoreFont.setColor(1, 0, 0, 1);
		scoreFont.draw(batch, "GAME OVER", x - 430, y + 200);
		
		scoreFont.setScale(1.3f);
		scoreFont.setColor(1, 1, 1, 1);
		scoreFont.draw(batch, "Your high score was " + level.score, 
								x - 250, y + 20);
		
		scoreFont.setColor(Color.YELLOW);
		scoreFont.draw(batch, "Tap to restart", x - 260, y - 80);
		
		batch.end();
	}
	
	private void saveHighScore() {
		Score newScore = new Score();
		Player player = new Player();
		player.name = "Player1";
		player.score = level.score;
		newScore.playersArray.add(player);
		
		Json json = new Json();
		FileHandle file = Gdx.files.local("scores.json");
		String stringScore = json.toJson(newScore);
		file.writeString(stringScore, true);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		batch.dispose();
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		
		game.setScreen(new GameScreen(game));
		
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		return false;
	}
	
	public static class Score {
		public ArrayList<Player> playersArray = new ArrayList<Player>();
	}

	public static class Player {
		public String name;
		public int score;
	}
}
