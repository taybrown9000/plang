package es.cleffongam.plang.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;

import es.cleffongam.plang.game.Assets;
import es.cleffongam.plang.game.Cameras;

public class AbstractGameScreen implements Screen {
	
	protected Game game;
	
	public AbstractGameScreen(Game game) {
		this.game = game;
	}

	@Override
	public void render(float deltaTime) {
	}

	@Override
	public void resize(int width, int height) {
		Cameras.instance.resize(width, height);
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
		Assets.instance.init(new AssetManager());
	}

	@Override
	public void dispose() {
		Assets.instance.dispose();
	}
}
