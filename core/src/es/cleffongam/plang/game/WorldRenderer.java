package es.cleffongam.plang.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Disposable;

import es.cleffongam.plang.util.Constants;

public class WorldRenderer implements Disposable {

	private ShapeRenderer shapeRenderer;
	private SpriteBatch spriteBatch;
	private BitmapFont guiFont;
	
	private WorldController worldController;
	
	boolean debugMode = true;
	
	public WorldRenderer (WorldController worldController) {
		this.worldController = worldController;
		init();
	}
	
	private void init() {
		shapeRenderer = new ShapeRenderer();
		spriteBatch = new SpriteBatch();
		
		guiFont = Assets.instance.fonts.guiFont;
		
	}
	
	public void render() {
		renderWorld(shapeRenderer);
		renderGUI(spriteBatch);
	}
	
	private void renderWorld(ShapeRenderer shapeRenderer) {
		shapeRenderer.setProjectionMatrix(
				Cameras.instance.worldCamera.combined);
		
		worldController.level.render(shapeRenderer);
	}
	
	private void renderGUI(SpriteBatch batch) {
		batch.setProjectionMatrix(
				Cameras.instance.guiCamera.combined);
		
		batch.begin();
		
		renderGUIScore(batch);
		
		// draw extra lives text
		renderGUILives(batch);
		
		// draw game over text
//		renderGUIGameOverMessage(batch);
		
		// draw debug stuff
		if (debugMode) {
			renderGUIFpsCounter(batch);
			renderGUIPaddleInfo(batch);
		}
		
		batch.end();
	}
	
	private void renderGUIScore(SpriteBatch batch) {
		float x = 3;
		float y = Cameras.instance.guiCamera.viewportHeight - 1;
		
		guiFont.setScale(1.2f);
		guiFont.draw(batch, "Score: " + worldController.level.score, x, y);
	}
	
	private void renderGUILives(SpriteBatch batch) {
		float x = Cameras.instance.guiCamera.viewportWidth - 150;
		float y = Cameras.instance.guiCamera.viewportHeight - 1;
		
		guiFont.setScale(1.2f);
		guiFont.draw(batch, "Lives: " + worldController.level.lives, x, y);
	}
	
	// DEBUG
	private void renderGUIFpsCounter(SpriteBatch batch) {
		float x = Cameras.instance.guiCamera.viewportWidth - 120;
		float y = 26;
		int fps = Gdx.graphics.getFramesPerSecond();
		
		if (fps >= 45) {
			//45 or more FPS show up in green
			guiFont.setColor(0, 1, 0, 1);
		} else if (fps >= 30) {
			// 30 or more FPS show up in yellow
			guiFont.setColor(1, 1, 0, 1);
		} else {
			// less than 30 FPS show up in red
			guiFont.setColor(1, 0, 0, 1);
		}
		
		guiFont.setScale(1);
		guiFont.draw(batch, "FPS: " + fps, x, y);
		guiFont.setColor(1, 1, 1, 1);	// white
	}
	
	// DEBUG
	private void renderGUIPaddleInfo(SpriteBatch batch) {
		float x = 1;
		float y = Cameras.instance.guiCamera.viewportWidth - 1;
		
		guiFont.setColor(Color.GREEN);
		
//		guiFont.draw(batch, "touchdrag Y: " + 
//				worldController.touchDragCoords.y, x, y);
//		
//		guiFont.draw(batch, "firsttouch Y: " + 
//				worldController.initialTouchCoords.y, x, y - 25);
		
		guiFont.setColor(1, 1, 1, 1);	// white
	}
	
	@Override
	public void dispose() {
		shapeRenderer.dispose();
		spriteBatch.dispose();
	}

}
