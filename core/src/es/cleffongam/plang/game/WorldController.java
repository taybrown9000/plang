package es.cleffongam.plang.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import es.cleffongam.plang.screens.GameOverScreen;

public class WorldController extends InputAdapter implements GestureListener {

	private static final String TAG = WorldController.class.getName();
	
	private Game game;
	public Level level;
	
	public Vector3 touchDragCoords;
	public Vector3 initialTouchCoords;
	
	public WorldController(Game game) {
		this.game = game;
		init();
	}
	
	private void init() {
		GestureDetector gd = new GestureDetector(this);
		Gdx.input.setInputProcessor(new InputMultiplexer(this, gd));
		
		touchDragCoords = new Vector3();
		initialTouchCoords = new Vector3();
		
		level = new Level();
	}
	
	private void backToMenu() {
		//switch to menu screen
//		game.setScreen(new MenuScreen(game));
	}
	
	public void update() {
		// handle user input
		handleInputGame();
		
		// update the level
		level.update();
		
		// test for object collisions
		level.checkCollisions();
		
		
	}
	
	public boolean isGameOver() {
		return level.lives < 0;
	}
	
	// Input handling -----------------------------------------------
	
	private void handleInputGame() {

		// if the W or S key is pressed, move accordingly
		if (Gdx.input.isKeyPressed(Keys.W)) {
			level.paddle.moveUp();
		} else if (Gdx.input.isKeyPressed(Keys.S)) {
			level.paddle.moveDown();
		} else if (Gdx.input.isKeyPressed(Keys.SPACE)) {
			level.ball.isAttached = false;
		}
				
	}

	@Override
	public boolean keyUp (int keycode) {
		// Reset game world
		if (keycode == Keys.R) {
			init();
		}
		
		// Back to Menu
		else if (keycode == Keys.ESCAPE || keycode == Keys.BACK) {
//			backToMenu();
			Gdx.app.exit();
		}
		
		return false;
	}
	

	// InputAdapter touch controls ------------------------------------------------
	
	// records the coords of the touch once it has been held down and dragged
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		
		// unproject camera to get the right y-axis values
		touchDragCoords.set(screenX, screenY, 0);
		Cameras.instance.worldCamera.unproject(touchDragCoords);
		
		level.paddle.position.y -= level.paddle.originPosition.y - touchDragCoords.y;
		
		level.collidePaddle_Window();
		
		return false;
	}

	// records the coords of the initial touch on the screen
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		return false;
	}
	
	// GestureListener -------------------------------------------------------

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		
		// detach ball on tap
		level.ball.isAttached = false;
		
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		return false;
	}
	
}
