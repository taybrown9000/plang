package es.cleffongam.plang.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Disposable;

public class Assets implements Disposable, AssetErrorListener {
	
	public static final String TAG = Assets.class.getName();
	public static final Assets instance = new Assets();
	
	private AssetManager assetManager;
	
	public AssetFonts fonts;
	public AssetSounds sounds;
	public AssetMusic music;
	
	// singleton: prevent instantiation from other classes
	private Assets() {}
	
	public void init(AssetManager assetManager) {
		this.assetManager = assetManager;
		
		assetManager.setErrorListener(this);
		
		// load texture atlas
		
		
		// load sounds
		assetManager.load("sounds/ballCollision.ogg", Sound.class);
		assetManager.load("sounds/paddleLaser.ogg", Sound.class);
		assetManager.load("sounds/paddleHurt.ogg", Sound.class);
		assetManager.load("sounds/gameOver.ogg", Sound.class);
		
		assetManager.load("sounds/8.mp3", Music.class);
		
		// start loading assets and wait until finished
		assetManager.finishLoading();
		
		fonts = new AssetFonts();
		sounds = new AssetSounds(assetManager);
		music = new AssetMusic(assetManager);
	}
	
	
	@SuppressWarnings("rawtypes")
	@Override
	public void error(AssetDescriptor asset, Throwable throwable) {
		Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName +
				"'", (Exception) throwable);
	}

	@Override
	public void dispose() {
		assetManager.dispose();
		fonts.guiFont.dispose();
	}

	public class AssetSounds {
		public final Sound ballWindowCollision;
		public final Sound ballPaddleCollision;
		public final Sound paddleLaser;
		public final Sound paddleHurt;
		public final Sound gameOver;
		
		public AssetSounds(AssetManager am) {
			ballWindowCollision = am.get("sounds/ballCollision.ogg", Sound.class);
			ballPaddleCollision = ballWindowCollision;
			
			paddleLaser = am.get("sounds/paddleLaser.ogg", Sound.class);
			paddleHurt = am.get("sounds/paddleHurt.ogg", Sound.class);
			gameOver = am.get("sounds/gameOver.ogg", Sound.class);
		}
		
	}

	public class AssetMusic {
		public final Music bg_music;
		
		public AssetMusic(AssetManager am) {
			bg_music = am.get("sounds/8.mp3", Music.class);
		}
	}
	
	public class AssetFonts {
		
		public final BitmapFont guiFont;
		
		public AssetFonts() {
			guiFont = new BitmapFont(Gdx.files.internal("fonts/mini_square.fnt"));
			
			// set font sizes
//			guiFont.setScale(0.50f);
			
		}
	}
}
