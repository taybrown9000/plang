package es.cleffongam.plang.game;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

import es.cleffongam.plang.game.objects.Ball;
import es.cleffongam.plang.game.objects.Enemies;
import es.cleffongam.plang.game.objects.Enemies.Enemy;
import es.cleffongam.plang.game.objects.Lasers;
import es.cleffongam.plang.game.objects.Lasers.Laser;
import es.cleffongam.plang.game.objects.Paddle;
import es.cleffongam.plang.game.objects.Stars;
import es.cleffongam.plang.util.Constants;

public class Level {
	
	public static final String TAG = Level.class.getName();
	
	private Rectangle obj1 = new Rectangle();
	private Rectangle obj2 = new Rectangle();
	
	// objects
	public Paddle paddle;
	public Ball ball;
	public Enemies enemies;
	public Lasers lasers;
	
	// decoration
	private Stars stars;
	
	// collision variables
	private float diffX, diffY;
	private float velocityScale = 1f;  	// This will scale how much you want the velocity to change
	private float maxVel = 10f; 
	private float maxDiff;
	
	public int lives = Constants.lives;
	public int score = 0;
	private float timeLeftGameOverDelay;

	private boolean paddleHit = false;
	
	public Level() {
		init();
	}
	
	private void init() {
		// decoration
		stars = new Stars();
		
		// objects
		paddle = new Paddle();
		ball = new Ball(paddle);
		lasers = new Lasers(paddle, ball);
		enemies = new Enemies(lasers);
		
		maxDiff = paddle.dimension.y/2 + ball.dimension.y/2;
	}
	
	public void render(ShapeRenderer shapeRenderer) {
		shapeRenderer.begin(ShapeType.Filled);
		
		stars.render(shapeRenderer);
		lasers.render(shapeRenderer);
		
		// damage color
		if (paddleHit) {
			shapeRenderer.setColor(1, 0, 0, 1);
			paddleHit = false;
		} else {
			paddle.render(shapeRenderer);
		}
		
		ball.render(shapeRenderer);
		enemies.render(shapeRenderer);
		
		shapeRenderer.end();
	}
	
	public void update() {
		
		stars.update();
		
		paddle.update();
		ball.update();
		lasers.update();
		enemies.update();
	}

	public void checkCollisions() {
		// collide paddle and ball object with the window
		collidePaddle_Window();
		collideBall_Window();
		
		// collide the paddle and ball
		collidePaddle_Ball();
		
		// collide ball and enemies
		collidePaddle_Enemies();
		collideBall_Enemies();
		
		// checking lasers
		collideLaser_Enemies();
		collidePaddle_EnemyLasers();
	}

	private void collidePaddle_EnemyLasers() {
		obj1.set(paddle.position.x, paddle.position.y, 			// paddle
				paddle.bounds.width, paddle.bounds.height);

		for (Laser laser : lasers.activeEnemyLasers) {
			obj2.set(laser.position.x, laser.position.y,
					laser.dimension.x, laser.dimension.y);

			if (obj1.overlaps(obj2)) {
				laser.alive = false;
				lives--;
				paddleHit  = true;
				Assets.instance.sounds.paddleHurt.play(1.2f, 0.6f, 0);
			}
		}
	}

	private void collideLaser_Enemies() {
		for (Laser laser : lasers.activePaddleLasers) {
			obj1.set(laser.position.x, laser.position.y,
					laser.dimension.x, laser.dimension.y);
			
			for (Enemy enemy : enemies.activeEnemies) {
				obj2.set(enemy.bounds.x, enemy.bounds.y,
						enemy.bounds.width, enemy.bounds.height);
				
				if (obj1.overlaps(obj2)) {
					laser.alive = false;
					enemy.alive = false;
					score += 100;
				}
			}
		}
	}

	private void collidePaddle_Enemies() {
		obj1.set(paddle.position.x, paddle.position.y, 			// paddle
				paddle.bounds.width, paddle.bounds.height);
		
		for (Enemy enemy : enemies.activeEnemies) {
			obj2.set(enemy.bounds.x, enemy.bounds.y,
					enemy.bounds.width, enemy.bounds.height);
			
			if (obj1.overlaps(obj2)) {
				lives--;
				enemy.alive = false;
			}
		}
	}

	private void collideBall_Enemies() {
		obj1.set(ball.position.x, ball.position.y, 				// ball
				ball.bounds.width, ball.bounds.height);
		
		for (Enemy enemy : enemies.activeEnemies) {
			obj2.set(enemy.bounds.x, enemy.bounds.y,
					enemy.bounds.width, enemy.bounds.height);
			
			if (obj1.overlaps(obj2)) {
				enemy.alive = false;
				score += 200;
			}
		}
	}

	private void collidePaddle_Ball() {
		// set collision rectangles to respective object bounds
		obj1.set(paddle.position.x, paddle.position.y, 			// paddle
				paddle.bounds.width, paddle.bounds.height);
		obj2.set(ball.position.x, ball.position.y, 				// ball
				ball.bounds.width, ball.bounds.height);
		
		// front of paddle
		if (obj2.overlaps(obj1)) {
			// It overlaps, we can give the ball an outgoing velocity vector based on the position 
          	// difference between the center of the paddle and the center of the ball
			diffX = obj2.x - obj1.x;
			diffY = ball.origin.y - paddle.origin.y;
			
			//Right at the top or bottom, with a 5% margin of error
			if (Math.abs(diffY) > maxDiff * 0.95f) {
				//reflect
				ball.position.y = diffY > 0 ? paddle.position.y + paddle.bounds.height + ball.bounds.height/2 : 
												paddle.position.y - ball.bounds.height/2;
				ball.velocity.y = -ball.velocity.y;
				//Otherwise alter the velocity
			} else {
				// If the ball hits at the edges of the paddle, the velocity gets changed by +/- velocityScale
				ball.velocity.y += (diffY/maxDiff) * velocityScale;
	            ball.velocity.y = MathUtils.clamp(ball.velocity.y, -maxVel, maxVel);
			}
			
			//Left paddle-ball set
			if (diffX >= 0) {  
				ball.position.x = obj1.x + obj1.width; 
				ball.velocity.x = -ball.velocity.x; 
				
			//Right paddle-ball set
			} else {
				ball.position.x = obj1.x - ball.dimension.x;
				ball.velocity.x = -ball.velocity.x;
			}
			
			Assets.instance.sounds.ballPaddleCollision.play(1f, 1.5f, 0f);
        }
		
	}

	private void collideBall_Window() {
		
		// if the ball collides with the right side of the window
		if ((ball.position.x + ball.dimension.x) > Constants.WORLD_WIDTH) {
			ball.position.x = Constants.WORLD_WIDTH - ball.dimension.x;
			ball.velocity.x = -ball.velocity.x;
			Assets.instance.sounds.ballWindowCollision.play();
		}
		
		// if the ball collides with the top of the window
		else if ((ball.position.y + ball.dimension.y) > Constants.WORLD_HEIGHT)
		{
			ball.position.y = Constants.WORLD_HEIGHT - ball.dimension.y;
			ball.velocity.y = -ball.velocity.y;
			Assets.instance.sounds.ballWindowCollision.play();
		}
		
		// bottom of window
		else if (ball.position.y < 0) {
			ball.position.y = 0;
			ball.velocity.y = -ball.velocity.y;
			Assets.instance.sounds.ballWindowCollision.play();
		}
		
		if (ball.isOffscreen()) {
			lives--;
			Assets.instance.sounds.paddleHurt.play(1.2f, 0.6f, 0);
			paddleHit = true;
		}
		
	}

	public void collidePaddle_Window() {
		// if the height of the paddle is colliding w/ the top of the window
		if (paddle.position.y + paddle.dimension.y > Constants.WORLD_HEIGHT)
			paddle.position.y = Constants.WORLD_HEIGHT - paddle.dimension.y;

		// if paddle's bottom tries to pass the bottom of the window
		else if (paddle.position.y < 0)
			paddle.position.y = 0;
	}
	

}
