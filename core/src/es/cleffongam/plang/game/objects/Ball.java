package es.cleffongam.plang.game.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Ball extends AbstractGameObject {

	Paddle paddle;
	
	private float width = 28f;
	private float height = 28f;
	private float x;
	private float y;
	
	public boolean isAttached = true;
	
	public Ball(Paddle paddle) {
		this.paddle = paddle;
		init();
	}
	
	private void init() {
		
		// put the paddle in front of the paddle on x-axis
		x = (paddle.position.x + paddle.dimension.x) + 20;
		// and in the center of the paddle on the y-axis
		y = (paddle.position.y + paddle.origin.y) - (height / 2);
		
		// set dimensions
		dimension.set(width, height);
		
		// Center image on game object
		origin.set(dimension.x / 2, dimension.y / 2);
		
		// Position on screen
		position.set(x, y);
		
		// Initialize physics values
		velocity.set(5, 5);
	}
	
	public boolean isOffscreen() {
		
		// if off-screen
		if (position.x < -dimension.x) {
			position.set(x, y);
			isAttached = true;
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void update() {
		
		
		// if the ball is still attached to the paddle
		if (isAttached) {
			position.y = paddle.originPosition.y - (height / 2);
		}
		// detached from paddle; move and collide
		else
			super.update();
			
		bounds.set(position.x, position.y, dimension.x, dimension.y);
	}

	@Override
	public void render(ShapeRenderer shapeRenderer) {
		
		shapeRenderer.setColor(Color.CYAN);
		
		shapeRenderer.rect(position.x, position.y, 
							dimension.x, dimension.y, 
							origin.x, origin.y, 
							rotation);
		
		shapeRenderer.setColor(1, 1, 1, 1);
	}

}
