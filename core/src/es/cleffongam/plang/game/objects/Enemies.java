package es.cleffongam.plang.game.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Timer;

import es.cleffongam.plang.game.objects.Lasers.Laser;
import es.cleffongam.plang.util.Constants;

public class Enemies extends AbstractGameObject {
	
	public final Array<Enemy> activeEnemies;
	public final Pool<Enemy> enemyPool;
	
	private Lasers lasers;

	public class Enemy extends AbstractGameObject implements Poolable {
		
		private float width = 80f;
		private float height = 40f;
		private float x = Constants.WORLD_WIDTH + 20;
		private float y = MathUtils.random(20, Constants.WORLD_HEIGHT - 20);
		private float xSpeed = -1.3f;
		
		public boolean alive = true;
		
		// Enemy constructor
		public Enemy() {
			// set dimensions
			dimension.set(width, height);
			
			// set origin
			origin.set(dimension.x/2, dimension.y/2);
			
			// create in the middle of the world
			position.set(x, y);
			
			velocity.set(xSpeed, 0);
		}
		
		@Override
		public void reset() {
			// create in the middle of the world
			position.set(x, y);
			alive = true;
		}
		
		@Override
		public void update() {
			super.update();
			
			if (position.x < -dimension.x)
				alive = false;
			
			bounds.set(position.x, position.y - dimension.y, 
					dimension.x, dimension.y * 2);
		}

		@Override
		public void render(ShapeRenderer shapeRenderer) {
			
			shapeRenderer.triangle(position.x, position.y, 
					position.x + dimension.x, position.y + dimension.x/2, 
					position.x + dimension.x, position.y - dimension.x/2,
					Color.ORANGE, Color.RED, Color.RED);
			
			// draw bounds
//			shapeRenderer.end();
//			shapeRenderer.begin(ShapeType.Line);
//			shapeRenderer.setColor(1, 1 ,1 ,1);
//			shapeRenderer.rect(bounds.x, bounds.y, bounds.width, bounds.height);
//			shapeRenderer.end();
//			
//			shapeRenderer.begin(ShapeType.Filled);
		}
	}
	
	public Enemies(final Lasers lasers) {
		this.lasers = lasers;
		
		// create array
		activeEnemies = new Array<Enemy>();
		// create pool of enemies
		enemyPool = new Pool<Enemy>() {
			@Override
			protected Enemy newObject() {
				return new Enemy();
			}
		};

		Timer.Task task = new Timer.Task() {
			@Override
			public void run() {
				final Enemy enemy = enemyPool.obtain();
				
				Timer.Task enemyLaserTask = new Timer.Task() {
					@Override
					public void run() {
						Laser laser = new Laser(enemy);
						lasers.activeEnemyLasers.add(laser);
					}
				};
				
				Timer.schedule(enemyLaserTask, 2, 3);
				
				activeEnemies.add(enemy);
			}
		};

		Timer.schedule(task, 1.5f, MathUtils.random(1.5f, 3));
	}
	
	private void checkIfAlive(int index, Enemy enemy) {
		if (!enemy.alive) {
			activeEnemies.removeIndex(index);
			enemyPool.free(enemy);
		}
	}

	@Override
	public void update() {
		
		for (int i = activeEnemies.size - 1; i >= 0; i--) {
			Enemy enemy = activeEnemies.get(i);
			enemy.update();

			checkIfAlive(i, enemy);
		}
	}
	
	@Override
	public void render(ShapeRenderer shapeRenderer) {
		for (Enemy enemy : activeEnemies)
			enemy.render(shapeRenderer);
	}

}
