package es.cleffongam.plang.game.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;

import es.cleffongam.plang.game.Assets;
import es.cleffongam.plang.game.objects.Enemies.Enemy;
import es.cleffongam.plang.util.Constants;

public class Lasers extends AbstractGameObject {
	
	public enum LaserType { PADDLE, ENEMY };

	public final Array<Laser> activePaddleLasers;
	public final Array<Laser> activeEnemyLasers;
	
	private Paddle paddle;
	private Ball ball;
	
	public Timer.Task paddleTask;
	public Timer.Task enemyTask;
	
	public static class Laser extends AbstractGameObject {

		private LaserType laserType;
		public boolean alive = true;
		
		public Laser(Paddle paddle) {
			this.laserType = LaserType.PADDLE;
			
			velocity.set(4, 0);
			dimension.set(30, 8);
			position.set(paddle.position.x, paddle.originPosition.y);
		}
		
		public Laser(Enemy enemy) {
			this.laserType = LaserType.ENEMY;
			velocity.set(-2.6f, 0);
			dimension.set(8, 0);
			position.set(enemy.position.x + enemy.origin.x, 
						enemy.position.y + enemy.origin.y - 20);
		}
		
		@Override
		public void update() {
			super.update();
			
			if (position.x > Constants.WORLD_WIDTH)
				alive = false;
			
			bounds.set(position.x, position.y, dimension.x, dimension.y);
		}
		
		@Override
		public void render(ShapeRenderer shapeRenderer) {
			
			if (this.laserType == LaserType.ENEMY) {
				shapeRenderer.setColor(Color.RED);			// red
				shapeRenderer.circle(position.x, position.y, dimension.x);
			}
			
			else if (this.laserType == LaserType.PADDLE) {
				shapeRenderer.setColor(Color.YELLOW);		// yellow
				shapeRenderer.rect(position.x, position.y, dimension.x, dimension.y);
			}
		}
	}
	
	public Lasers(Paddle paddle, Ball ball) {
		this.paddle = paddle;
		this.ball = ball;
		
		activePaddleLasers = new Array<Laser>();
		activeEnemyLasers = new Array<Laser>();
		
		initPaddleLasers();
	}
	
	public void initPaddleLasers() {
		
		paddleTask = new Timer.Task() {
			@Override
			public void run() {
				
				if (!ball.isAttached) {
					Laser laser = new Laser(paddle);
					activePaddleLasers.add(laser);
					Assets.instance.sounds.paddleLaser.play(1, 1.5f, 0);
				}
			}
		};
		
		Timer.schedule(paddleTask, 0, 2);
	}
	
	private void checkIfAlivePaddleLasers(int index, Laser laser) {
		if (!laser.alive) {
			activePaddleLasers.removeIndex(index);
		}
	}
	
	private void checkIfAliveEnemyLasers(int index, Laser laser) {
		if (!laser.alive) {
			activeEnemyLasers.removeIndex(index);
		}
	}
	
	@Override
	public void update() {
		
		// paddle lasers
		for (int i = activePaddleLasers.size - 1; i >= 0; i--) {
			Laser laser = activePaddleLasers.get(i);
			laser.update();

			checkIfAlivePaddleLasers(i, laser);
		}
		
		// enemy lasers
		for (int i = activeEnemyLasers.size - 1; i >= 0; i--) {
			Laser laser = activeEnemyLasers.get(i);
			laser.update();

			checkIfAliveEnemyLasers(i, laser);
		}
		
	}
	
	@Override
	public void render(ShapeRenderer shapeRenderer) {
		
		// Paddle's lasers
		for (Laser laser : activePaddleLasers)
			laser.render(shapeRenderer);
		
		// Enemy lasers
		for (Laser laser : activeEnemyLasers)
			laser.render(shapeRenderer);
		
		shapeRenderer.setColor(1, 1, 1, 1);			// white
	}

}
