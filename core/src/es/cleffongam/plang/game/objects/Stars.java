package es.cleffongam.plang.game.objects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Timer;

import es.cleffongam.plang.util.Constants;

public class Stars extends AbstractGameObject implements Disposable {
	
	float scrollSpeed;
	float tintColor;
	float x, y, radius;

	public Array<Star> backgroundStars;
	private Array<Star> foregroundStars;
	
	private Pool<Star> fgPool;
	
	public class Star extends Circle implements Poolable {
		
		private float scrollSpeed;
		private float tint;
		
		// used for background
		public Star(float x, float y, float radius, float scrollSpeed, float tint) {
			this.x = x; this.y = y;
			this.radius = radius;
			this.scrollSpeed = scrollSpeed;
			this.tint = tint;
		}
		
		public void update() {
			x += scrollSpeed;
		}
		
		@Override
		public void reset() {
			x = Constants.WORLD_WIDTH + radius;
			y = MathUtils.random(Constants.WORLD_HEIGHT);
		}
		
		public void render(ShapeRenderer shapeRenderer) {
			shapeRenderer.setColor(tint, tint, tint, 1);
			shapeRenderer.circle(x, y, radius);
		}
	}
	
	public Stars() {
		createBackground();
		createForeground();
	}
	
	public boolean isOffScreen(Star star) {
		return star.x < -star.radius ? true : false;
	}
	
	private void createBackground() {
		backgroundStars = new Array<Star>();
		
		scrollSpeed = -0.5f;
		
		// create background layer
		for (int i = 0; i < 1000; i ++) {
			x = MathUtils.random(-2, Constants.WORLD_WIDTH + 2);
			y = MathUtils.random(-2, Constants.WORLD_HEIGHT + 2);
			radius = MathUtils.random(0.5f, 2.4f);
			tintColor = MathUtils.random(0.4f, 0.8f);
			
			Star star = new Star(x, y, radius, scrollSpeed, tintColor);
			backgroundStars.add(star);
		}
	}
	
	// create initial foreground star layer
	private void initialForeground() {
		
		Star star;
		
		for (int i = 0; i < 35; i++) {
			radius = MathUtils.random(0.6f, 2.1f);
			scrollSpeed = MathUtils.random(-0.6f, -5f);
			x = MathUtils.random(Constants.WORLD_WIDTH);
			y = MathUtils.random(Constants.WORLD_HEIGHT);
			
			star = new Star(x, y, radius, scrollSpeed, 1);
			
			foregroundStars.add(star);
		}
	}
	
	private void createForeground() {

		foregroundStars = new Array<Star>();
		
		initialForeground();
		
		fgPool = new Pool<Star>() {
			@Override
			protected Star newObject() {
				
				radius = MathUtils.random(0.6f, 2.1f);
				scrollSpeed = MathUtils.random(-0.6f, -5f);
				x = Constants.WORLD_WIDTH + radius;
				y = MathUtils.random(Constants.WORLD_HEIGHT);

				return new Star(x, y, radius, scrollSpeed, 1);
			}
		};

		Timer.Task task = new Timer.Task() {
			@Override
			public void run() {
				Star star = fgPool.obtain();
				foregroundStars.add(star);
			}
		};

		Timer.schedule(task, 1.5f, MathUtils.random(0.1f, 0.4f));
	}
	
	@Override
	public void update() {
		// update background
		for (Star star : backgroundStars) {
			star.update();
			
			if (isOffScreen(star))
				star.reset();
		}
		
		// update foreground
		for (int i = foregroundStars.size - 1; i >= 0; i--) {
			Star star = foregroundStars.get(i);
			star.update();

			// if outside of screen
			if (isOffScreen(star)) {
				foregroundStars.removeIndex(i);
				fgPool.free(star);
			}
		}
	}
	
	@Override
	public void render(ShapeRenderer shapeRenderer) {
		
		// draw background
		for (Star star : backgroundStars)
			star.render(shapeRenderer);
		
		for (Star star : foregroundStars)
			star.render(shapeRenderer);
		
		shapeRenderer.setColor(1, 1, 1, 1);
	}

	@Override
	public void dispose() {
		
		fgPool.freeAll(foregroundStars);
		fgPool.freeAll(backgroundStars);
		
		backgroundStars.clear();
		foregroundStars.clear();
	}

}
