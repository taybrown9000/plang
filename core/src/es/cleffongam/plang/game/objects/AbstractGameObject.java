package es.cleffongam.plang.game.objects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class AbstractGameObject {

	public Vector2 position;
	public Vector2 dimension;
	public Vector2 origin;
	public Vector2 scale;
	public Vector2 velocity;
	public Vector2 friction;
	public Vector2 acceleration;
	public Vector2 originPosition;
	
	public Rectangle bounds;
	
	public float rotation;
	
	public AbstractGameObject() {
		position = new Vector2();
		dimension = new Vector2(1, 1);
		origin = new Vector2();
		originPosition = new Vector2();
		scale = new Vector2(1, 1);
		rotation = 0;
		
		velocity = new Vector2();
		friction = new Vector2();
		acceleration = new Vector2();
		
		bounds = new Rectangle();
	}
	
	protected void updateMotionX() {
		
		// Apply acceleration
		velocity.x += acceleration.x;
	}
	
	protected void updateMotionY() {
		
		// Apply acceleration
		velocity.y += acceleration.y;
	}
	
	public void update() {
		updateMotionX();
		updateMotionY();
		
		// Move to new position
		position.x += velocity.x;
		position.y += velocity.y;
	}
	
	public abstract void render(ShapeRenderer shapeRenderer);
}
