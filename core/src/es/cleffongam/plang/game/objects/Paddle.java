package es.cleffongam.plang.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import es.cleffongam.plang.util.Constants;

public class Paddle extends AbstractGameObject {
	
	private float width = 28f;
	private float height = 140f;
	private float x = 110;
	private float y = (Constants.WORLD_HEIGHT / 2) - (height / 2);
	
	private boolean isRed = true;
	private float redTime = 0;
	
	public Paddle() {
		init();
	}
	
	private void init() {
		// set dimensions of paddle
		dimension.set(width, height);
		
		// Center image on game object
		origin.set(dimension.x / 2, dimension.y / 2);
		
		// Position on screen
		position.set(x, y);
		
		// Initialize physics values
		velocity.set(0, 0);
		friction.set(0, 5);
		acceleration.set(0, 1);
	}
	
	public void moveUp() {
		// create positive velocity
		velocity.y += acceleration.y;
	}
	
	public void moveDown() {
		// create negative velocity
		velocity.y -= acceleration.y;
	}
	
	@Override
	public void update() {
		
		// creating sliding motion w/ friction
		velocity.y *= (1 - Math.min(Gdx.graphics.getDeltaTime() * friction.y, 1));
		
		// Move to new position
		position.y += velocity.y * 1.5f;
		
		// Update originPosition
		originPosition.set(position.x + origin.x, position.y + origin.y);
		bounds.set(position.x, position.y, dimension.x, dimension.y);
	}

	@Override
	public void render(ShapeRenderer shapeRenderer) {
	
		shapeRenderer.rect(position.x, position.y, 
							dimension.x, dimension.y, 
							origin.x, origin.y, 
							rotation);
	}

}
