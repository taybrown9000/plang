package es.cleffongam.plang.game;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import es.cleffongam.plang.util.Constants;

public class Cameras {
	
	public static final String TAG = Camera.class.getName();
	public static final Cameras instance = new Cameras();

	public OrthographicCamera worldCamera;
	public OrthographicCamera guiCamera;
	
	private Viewport worldViewport;
	private Viewport guiViewport;
	
	// singleton
	private Cameras() {}
	
	public void init() {
		
		worldCamera = new OrthographicCamera(); 
		guiCamera = new OrthographicCamera();
		
		worldViewport = new StretchViewport(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT, 
											worldCamera);
		
		guiViewport = new ExtendViewport(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT, guiCamera);	
		
		worldCamera.update();
		guiCamera.update();
	}
	
	public void resize(int width, int height) {
		worldViewport.update(width, height, true);
		guiViewport.update(width, height, true);
	}
	
	
}
