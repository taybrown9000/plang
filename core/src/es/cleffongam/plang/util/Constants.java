package es.cleffongam.plang.util;

public class Constants {

	// width and height of the game world
	public static final float WORLD_WIDTH = 1280f;
	public static final float WORLD_HEIGHT = 800f;
	
	public static int lives = 3;
}
